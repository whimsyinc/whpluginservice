#
#  Be sure to run `pod spec lint WHPluginService.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see http://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |s|

  s.name         = "WHPluginService"
  s.version      = "0.1"
  s.summary      = "A plugin framework for Mac and iOS apps"

  s.description  = <<-DESC
                   A plugin framework for Mac and iOS apps.
                   
                   Stop building complex controllers and start making your apps modular.
                   DESC

  s.homepage     = "https://bitbucket.org/whimsyinc/whpluginservice"

  s.license      = { :type => 'MIT', :file => 'LICENSE' }

  s.author             = { "James Dumay" => "james.w.dumay@gmail.com" }
  s.social_media_url = "http://twitter.com/i386"

  #  When using multiple platforms
  s.ios.deployment_target = '5.0'
  s.osx.deployment_target = '10.7'


  # ――― Source Location ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  Specify the location from where the source should be retrieved.
  #  Supports git, hg, bzr, svn and HTTP.
  #

  s.source       = { :git => "https://i386@bitbucket.org/whimsyinc/whpluginservice.git", :tag => "0.0.1" }

  s.source_files  = 'PluginService/**/*.{h,m}'

  s.public_header_files = 'PluginService/**/*.h'

end

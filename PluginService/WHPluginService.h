//
//  PluginService.h
//  PluginService
//
//  Created by James Dumay on 16/03/2014.
//  Copyright (c) 2014 Whimsy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WHPlugin.h"

/**
 Plugin bundle loading service
 
 Subclass and add new methods that call protocol methods on the plugin instances of on the values of the plugin map.
 */
@interface WHPluginService : NSObject

/**
 Shared instance of the Plugin Service.
 
 On Mac, all plugins are loaded from Contents/PlugIns when it is called for the first time.
 */
+(instancetype)sharedService;

@property (readonly, strong) NSMutableDictionary *plugins;

/**
 Register a plugin instance
 
 Sutable for iOS where bundles can't be dynamically loaded.
 
 On Mac, bundles are loaded from Contents/PlugIns automatically when sharedService is called for the first time.
 
 It is not recommended to use this method on the Mac.
 */
-(void)registerPlugin:(NSString*)name plugin:(id<WHPlugin>)pluginInstance;

@end

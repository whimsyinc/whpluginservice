//
//  WHPlugin.h
//  PluginService
//
//  Created by James Dumay on 16/03/2014.
//  Copyright (c) 2014 Whimsy. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 Base plugin protocol
 
 Create your own protocol for all your apps plugins and make it conform to this protocol
 */
@protocol WHPlugin <NSObject>

@optional

/**
 Called when the plugin has been successfully reigstered
 */
-(void)pluginLoaded;

@end

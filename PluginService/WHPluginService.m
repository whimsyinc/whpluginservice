//
//  PluginService.m
//  PluginService
//
//  Created by James Dumay on 16/03/2014.
//  Copyright (c) 2014 Whimsy. All rights reserved.
//

#import "WHPluginService.h"

@implementation WHPluginService

+(instancetype)sharedService
{
    static dispatch_once_t pred;
    static WHPluginService *shared = nil;
    
    dispatch_once(&pred, ^{
        shared = [[WHPluginService alloc] init];
        [shared load];
    });
    
    return shared;
}

-(id)init
{
    if (self)
    {
        _plugins = [[NSMutableDictionary alloc] init];
    }
    return self;
}

-(void)registerPlugin:(NSString *)name plugin:(id<WHPlugin>)pluginInstance
{
    if ([_plugins objectForKey:name])
    {
        NSLog(@"There is already a plugin registered as '%@'. The instance will be replaced.", name);
    }
    
    if ([pluginInstance respondsToSelector:@selector(pluginLoaded)])
    {
        [pluginInstance pluginLoaded];
    }
    
    [_plugins setObject:pluginInstance forKey:name];
}

-(void)load
{
    NSMutableDictionary *plugins = [[NSMutableDictionary alloc] init];
    
    NSString *pluginPath = [[[NSBundle mainBundle] bundlePath] stringByAppendingPathComponent:@"Contents/PlugIns/"];
    
#if DEBUG
    NSLog(@"Exposure plugin path '%@'", pluginPath);
#endif
    
    NSDirectoryEnumerator *enumerator = [[NSFileManager defaultManager] enumeratorAtPath:pluginPath];
    
    for (NSString *path in enumerator)
    {
        if ([[path pathExtension] isEqualToString:@"bundle"])
        {
            NSString *pathToExposure = [pluginPath stringByAppendingPathComponent:path];
            [self loadBundle:pathToExposure plugins:plugins];
        }
    }
    
    _plugins = plugins;
}

-(void)loadBundle:(NSString*)path plugins:(NSMutableDictionary*)plugins
{
    Class pluginClass;
    id pluginInstance;
    NSBundle *bundleToLoad = [NSBundle bundleWithPath:path];
    
    //Should have a principalClass and confirm to WHPlugin
    if ((pluginClass = [bundleToLoad principalClass]) && [pluginClass conformsToProtocol:@protocol(WHPlugin)])
    {
        
#if DEBUG
        NSLog(@"Loaded '%@'", pluginClass);
#endif
        
        pluginInstance = [[pluginClass alloc] init];
        [plugins setObject:pluginInstance forKey:bundleToLoad.bundleIdentifier];
        
        if ([pluginInstance respondsToSelector:@selector(pluginLoaded)])
        {
            [pluginInstance pluginLoaded];
        }
    }
    else
    {
        NSLog(@"Could not load plugin class '%@' from bundle '%@'", bundleToLoad.principalClass, bundleToLoad.bundleIdentifier);
    }
}

@end

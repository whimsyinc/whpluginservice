//
//  WHMyPlugin.h
//  MyPlugin
//
//  Created by James Dumay on 16/03/2014.
//  Copyright (c) 2014 Whimsy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <BundleTestAppShared/BundleTestAppShared.h>

@interface WHMyPlugin : NSObject<WHTestPlugin>

@end

//
//  WHMyPlugin.m
//  MyPlugin
//
//  Created by James Dumay on 16/03/2014.
//  Copyright (c) 2014 Whimsy. All rights reserved.
//

#import "WHMyPlugin.h"

@implementation WHMyPlugin

-(void)windowControllerLoaded:(id<WHMainWindowController>)windowController
{
    NSLog(@"windowControllerLoaded");
    
    [windowController updateStatus:@"This content was defined by WHMyPlugin"];
}

@end

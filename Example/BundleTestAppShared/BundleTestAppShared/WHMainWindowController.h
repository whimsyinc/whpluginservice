//
//  WHMainWindowController.h
//  BundleTestAppShared
//
//  Created by James Dumay on 16/03/2014.
//  Copyright (c) 2014 Whimsy. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol WHMainWindowController <NSObject>

//Your API for your window controller goes here. Plugins should never know about the implementation detail and all communication should go thru this protocol

-(void)updateStatus:(NSString*)status;

@end

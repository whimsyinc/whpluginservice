//
//  WHBundleService.m
//  BundleTestAppShared
//
//  Created by James Dumay on 16/03/2014.
//  Copyright (c) 2014 Whimsy. All rights reserved.
//

#import "WHTestPluginService.h"
#import "WHTestPlugin.h"

@interface WHTestPluginService ()

@property (readonly, strong) NSDictionary *plugins;

@end

@implementation WHTestPluginService

+(void)windowControllerLoaded:(id<WHMainWindowController>)controller
{
    for (id<WHTestPlugin> plugin in [[[self sharedService] plugins] allValues])
    {
        if ([plugin respondsToSelector:@selector(windowControllerLoaded:)])
        {
            [plugin windowControllerLoaded:controller];
        }
    }
}

@end

//
//  WHPlugin.h
//  BundleTestAppShared
//
//  Created by James Dumay on 16/03/2014.
//  Copyright (c) 2014 Whimsy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WHPlugin.h"

@protocol WHTestPlugin <WHPlugin>

@optional

-(void)windowControllerLoaded:(id<WHMainWindowController>)windowController;

@end

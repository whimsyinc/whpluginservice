//
//  WHBundleService.h
//  BundleTestAppShared
//
//  Created by James Dumay on 16/03/2014.
//  Copyright (c) 2014 Whimsy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WHPluginService.h"
#import "WHMainWindowController.h"

@interface WHTestPluginService : WHPluginService

+(void)windowControllerLoaded:(id<WHMainWindowController>)controller;

@end

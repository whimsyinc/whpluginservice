//
//  WHAppDelegate.m
//  BundleTestApp
//
//  Created by James Dumay on 16/03/2014.
//  Copyright (c) 2014 Whimsy. All rights reserved.
//

#import "WHAppDelegate.h"
#import "WHMainWindowController.h"

@interface WHAppDelegate ()

@property (strong) WHMainWindowController *controller;

@end

@implementation WHAppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    //Create the controller
    _controller = [[WHMainWindowController alloc] init];
    
    //Show it
    [_controller.window makeKeyAndOrderFront:self];
    
    //Let plugins have their way with it via the protocol
    [WHTestPluginService windowControllerLoaded:_controller];
}

@end

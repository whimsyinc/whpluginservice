//
//  WHMainWindowController.h
//  BundleTestApp
//
//  Created by James Dumay on 16/03/2014.
//  Copyright (c) 2014 Whimsy. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface WHMainWindowController : NSWindowController <WHMainWindowController>

@property (weak) IBOutlet NSTextField *statusLabel;

@end

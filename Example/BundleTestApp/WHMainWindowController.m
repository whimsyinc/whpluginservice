//
//  WHMainWindowController.m
//  BundleTestApp
//
//  Created by James Dumay on 16/03/2014.
//  Copyright (c) 2014 Whimsy. All rights reserved.
//

#import "WHMainWindowController.h"

@interface WHMainWindowController ()

@end

@implementation WHMainWindowController

-init
{
    self = [super initWithWindowNibName:@"WHMainWindowController"];
    if (self)
    {
        
    }
    return self;
}

- (void)windowDidLoad
{
    [super windowDidLoad];
    
    // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
}

-(void)updateStatus:(NSString *)status
{
    _statusLabel.stringValue = status;
}

@end

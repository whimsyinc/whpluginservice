//
//  main.m
//  BundleTestApp
//
//  Created by James Dumay on 16/03/2014.
//  Copyright (c) 2014 Whimsy. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[])
{
    return NSApplicationMain(argc, argv);
}
